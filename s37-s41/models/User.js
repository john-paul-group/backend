const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : (true, "firstName is required")
	},
	lastName : {
		type : String,
		required : (true, "lastName is required")
	},
	email : {
		type : String,
		required : (true, "EMAIL is required")
	},
	password : {
		type : String,
		required : (true, "PASSWORD is required")
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : Number,
		required : (true, "Mobile No. is required")
	},
	enrollments : [
			{
				courseId : {
					type : String,
					required : (true, "Course ID is required")
				},
				enrolledOn : {
					type : Date,
					default : new Date()
				},
				status : {
					type : String,
					default : "Enrolled"
				}
				
			},
			
		]

})

module.exports = mongoose.model("User", userSchema);