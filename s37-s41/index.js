// SERVER CREATION AND DB CONNECTION
const express = require("express");
const mongoose = require("mongoose");
// ALLOWS US TO CONTROL THE APP CORS
const cors = require("cors")
const userRoutes = require("./routes/user.js");
const courseRoutes = require("./routes/course")


const app = express();


mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.vwcmrqs.mongodb.net/Course_Booking_System?retryWrites=true&w=majority",
		{
			useNewUrlParser : true,
			useUnifiedTopology : true
		}
	);

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));





// Define the "/users" string to be included for the user routes defined in the "user.js" routes file
app.use("/users" , userRoutes);
app.use("/courses", courseRoutes);



// PORT LISTENING
if(require.main === module){
	app.listen(process.env.PORT || 4000, () => console.log(`API is now online on port ${process.env.PORT || 4000}`))
}

module.exports = app;