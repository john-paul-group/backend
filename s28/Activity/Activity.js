db.rooms.insertOne({

name: "Single",
accomodates: 3,
price: 1000,
description: "A simple room with all basic necessities",
rooms_available: 10,
isAvailable: false

});

db.rooms.insertMany([
{

	name: "Double",
	accomodates: 3,
	price: 2000,
	description: "A room fit for small going on a vacation",
	rooms_available: 5,
	isAvailable: false


},

{

	name: "Queen",
	accomodates: 4,
	price: 4000,
	description: "A room with queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false


}


]);

db.rooms.find({name: "Double"});

db.rooms.updateOne(
	{rooms_available: 15},

	{

		$set: {
		rooms_available: 0,
		}

	}
);

db.users.deleteMany({
		rooms_available: 0
});