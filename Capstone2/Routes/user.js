const express = require("express");
const router = express.Router();
const userController = require("../Controller/user.js")
const auth = require("../auth.js")
const Coffee = require("../Model/Coffee.js")

router.post("/register", (req, res) => {
	userController.registeredUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/create", (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.createOrder(data).then(resultFromController => res.send(resultFromController));
})
module.exports = router;

router.get("/all", (req, res) => {
	userController.getAllUserDetails().then(resultFromController => res.send(resultFromController));
});

router.put("/:userId/updateAdmin", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.updatingAdmin(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
	
});