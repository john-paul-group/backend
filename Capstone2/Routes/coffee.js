const express = require("express");
const router = express.Router();
const coffeeController = require("../Controller/coffeeController.js");
const auth = require("../auth.js");

router.post("/", auth.verify, (req, res) => {
	const data = {
		coffee: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	coffeeController.addCoffeMenu(data).then(resultFromController => res.send(resultFromController));
});

// GET ALL COFFEE MENU
router.get("/all", (req, res) => {
	coffeeController.getAllCoffeeMenu().then(resultFromController => res.send(resultFromController));
});

// GET ALL ACTIVE COFFEE MENU (ADMIN ONLY)
router.get("/", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	coffeeController.getAllActiveCoffee(isAdmin).then(resultFromController => res.send(resultFromController));
})

// GET SINGLE COFFEE PRODUCT
router.get("/:coffeeId", (req, res) => {
	console.log(req.params.coffeeId);
	coffeeController.getSingleCoffee(req.params).then(resultFromController => res.send(resultFromController));

})

// UPDATING COFFEE INFORMATION
router.put("/:coffeeId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	coffeeController.updateCoffeeInformation(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));

})

// ARCHIVED COFFEE MENU
router.put("/:coffeeId/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	coffeeController.coffeeMenuArchive(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
	
});

// ACTIVATED COFFEE MENU
router.put("/:coffeeId/activate", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	coffeeController.coffeeMenuActivate(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
	
});

module.exports = router;
