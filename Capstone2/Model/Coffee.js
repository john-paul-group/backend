const mongoose = require("mongoose");

const coffeeSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "PRODUCT NAME is required!"]
	},
	description : {
		type : String,
		required : [true, "PRODUCT DESCRIPTION is required!"]
	},
	price : {
		type : Number,
		required : [true, "PRICE is required!"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date(2023, 5)
	},
	userOrders : [
		 	{
		 		userId :{
		 			type : Object,
		 			required : [true, "USER I.D is required"]
		 		},
		 	}
		]
})


module.exports = mongoose.model("Coffee", coffeeSchema);
