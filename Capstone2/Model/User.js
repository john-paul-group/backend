const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	email : {
		type : String,
		required : (true, "EMAIL is required")
	},
	password : {
		type : String,
		required : (true, "PASSWORD is required")
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : Number,
		required : (true, "Mobile No. is required")
	},
	orderedProducts : [
			{
				productId : {
					type : String,
					required : (true, "Course ID is required")
				},
				productName : {
					type : String,
					required : (true, "PRODUCT NAME is required")
				},
				quantity : {
					type : Number,
					required : [true, "You must enter the quantity"]
				},
				totalAmount : {
					type : Number,
					required : [true, "Total amount is required"]
				}
				
			},
			
		]

})

module.exports = mongoose.model("User", userSchema);
