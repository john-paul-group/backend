const Coffee = require("../Model/Coffee");

// CREATING COFFEE MENU
module.exports.addCoffeMenu = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newCoffee = new Coffee({
			name: data.coffee.name,
			description: data.coffee.description,
			price: data.coffee.price
		});

		return newCoffee.save().then((coffee, error) => {
			if(error){
				return error;
			}
			return coffee;
		})
	} 

	let message = Promise.resolve("Not an Admin! Cannot access right now.");

	return message.then((value) => {
		return value
	})
}

// GET ALL COFFEE MENU
module.exports.getAllCoffeeMenu = () => {
	return Coffee.find({}).then(result => {
		return result;
	})
}

// GET ALL ACTIVE MENU (ADMIN ONLY)
module.exports.getAllActiveCoffee = (isAdmin) => {
 if (isAdmin){
 	return Coffee.find({isActive : true}).then(result => {
		return result;
	})
 }

 	let message = Promise.resolve("Not an Admin! Cannot access right now.");

	return message.then((value) => {
		return value
	})
	
}

// GET SPECIFIC COFFEE MENU
module.exports.getSingleCoffee = (reqParams) => {
	return Coffee.findById(reqParams.coffeeId).then(result =>{
		return result;
	})
}


// UPDATING COFFEE INFORMATION

module.exports.updateCoffeeInformation = (reqParams, reqBody, isAdmin) => {

	if(isAdmin){
		let coffeeInfo = {

		description : reqBody.description,
		price : reqBody.price

		}

		return Coffee.findByIdAndUpdate(reqParams.coffeeId, coffeeInfo).then((coffee, error) => {
			if(error){
				return false;
			}else{
				return "Your Coffee Information is Updated!";
			}
		})
	}

	let message = Promise.resolve("Not an Admin! Cannot access right now.");

	return message.then((value) => {
		return value
	})
	
}

// ARCHIVING COFFEE MENU
module.exports.coffeeMenuArchive = (reqParams, isAdmin) => {

	let activeArchive = {
		isActive : false
	};

	if(isAdmin){
		return Coffee.findByIdAndUpdate(reqParams.coffeeId, activeArchive).then((coffee, error) => {

			
			if (error) {
				return false;
		
			} else {
				return true;
			}
		});	
	}

	let message = Promise.resolve("Not an Admin! Cannot access right now.");

	return message.then((value) => {
		return value
	})
}

// ACTIVATE COFFEE MENU
module.exports.coffeeMenuActivate = (reqParams, isAdmin) => {

	let activeField = {
		isActive : true
	};

	if(isAdmin){
		return Coffee.findByIdAndUpdate(reqParams.coffeeId, activeField).then((coffee, error) => {

			
			if (error) {
				return false;
		
			} else {
				return true;
			}
		});	
	}

	let message = Promise.resolve("Not an Admin! Cannot access right now.");

	return message.then((value) => {
		return value
	})
}
