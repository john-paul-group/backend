const User = require("../Model/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Coffee = require("../Model/Coffee.js");
const coffeeController = require("../Controller/coffeeController.js");


module.exports.registeredUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if (error){
			return false;
		}else{
			return true;
		}
	})
};


module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result =>  {
		console.log(result)

		if(result == null){
			return false
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			console.log(isPasswordCorrect)
			if(isPasswordCorrect){
				return{access: auth.createAccessToken(result)}
			} else{
				return false
			}
		}
	})


}

module.exports.createOrder = async (data) => {
	if(data.isAdmin){
		return "You are not allowed"
	}else{
		let isUserUpdated = await User.findById(data.userId).then(user => {
            let isOrderedProductUpdated = Coffee.findById(data.productId).then( product => {
                user.orderedProducts.push(
                    {
                        product : [
                            {
                                productId : data.productId,
                                productName: product.name,
                                quantity : data.quantity
                            }
                        ],
                        totalAmount : data.coffee.price * data.quantity
                    }
                );
                return user.save().then((user, error) => {
                    if(error){
                        return false;
                    }else{
                        return true;
                    }
                })
            })
            if(isOrderedProductUpdated){
                return Promise.resolve(true);
            } else {
                return Promise.resolve(false);
            }
       	 })

			let isCoffeeUpdated = await Coffee.findById(data.productId).then(product => {
				product.userOrders.push({userId : data.userId});
				return product.save().then((product, error) => {
			if(error){
				return false;
				}else{
				return true;
				}
			})
		})
			if(isUserUpdated && isCoffeeUpdated){
			return "Thank your for your purchase! Enjoy your Coffee";
		}else{
			return "Something went wrong with your request. Please try again later!";
		}
	}
}

module.exports.getAllUserDetails = () => {
	return User.find({}).then(result => {
		return result;
	})
}

// STRETCH GOALS
module.exports.updatingAdmin = (reqParams, reqBody, isAdmin) => {
	let updateAdminField = {
		isAdmin : reqBody.isAdmin
	};

	if (isAdmin){
		return User.findByIdAndUpdate(reqParams.userId, updateAdminField).then((user, error) => {
			if (error) {
				return false;
			}else{
				return true;
			}
		})
	}

	let message = Promise.resolve("Not an Admin! Cannot access right now.");

	return message.then((value) => {
		return value
	})


}